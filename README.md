# Tauri + Angular

This template should help get you started developing with Tauri and Angular.

## Recommended IDE Setup

[VS Code](https://code.visualstudio.com/) + [Tauri](https://marketplace.visualstudio.com/items?itemName=tauri-apps.tauri-vscode) + [rust-analyzer](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer) + [Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template).

## 环境搭建

原文地址: https://tauri.app/zh-cn/v1/guides/getting-started/prerequisites

### Windows

- Microsoft C++ 生成工具

https://visualstudio.microsoft.com/zh-hans/visual-cpp-build-tools/

- WebView2

https://developer.microsoft.com/zh-cn/microsoft-edge/webview2/?form=MA13LH#download-section

- Rust

https://www.rust-lang.org/tools/install

### macOS

- CLang 和 macOS 开发依赖项

```shell
xcode-select --install
```

- Rust

```shell
curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh
```
